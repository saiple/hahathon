var express = require('express');
const axios = require('axios');
bpmURL = "http://188.120.238.74:8085/api/v1/";


bodyParser = require('body-parser').json();
const fs = require('fs');
const pg        = require('pg');
const app       = express();

const config = {
    user: 'postgres',
    database: 'taskmanager',
    password: 'r1a2z3i4l5',
    port: 5432
};
app.set('view engine', 'ejs');
const pool = new pg.Pool(config);
module.exports = function(app, fs) {
    /* GET home page. */
    app.get('/kjb', function (req, res, next) {

        pool.connect(function (err, client, done) {
            if (err) {
                console.log("Can not connect to the DB" + err);
            }
            client.query('SELECT * FROM Employee', function (err, result) {
                done();
                if (err) {
                    console.log(err);
                    res.status(400).send(err);
                }
                res.status(200).send(result.rows);
            })
        })

    });

    app.get('/', function (req, res) {
        res.render('starting_page.ejs');
    });

    app.get('/user', function (req, res) {
        var url = require('url');
        var query = url.parse(req.url, true).query;
        // res.redirect("/user?id=dad159f3-6c2d-446a-98d2-0f4d26662bbe");

        console.log(req.cookies);
        console.log(req.url);
        console.log(query);

        var id = query.id ? query.id : "dad159f3-6c2d-446a-98d2-0f4d26662bbe";
        console.log(id);

        axios.defaults.headers.common['Authorization'] = 'Bearer ' + req.cookies.Authorization;
        axios.post(bpmURL + "readMultiple", {
                filterConnector: {
                    "subFiltersConnection": "And",
                    "subFilters":
                        [
                            {
                                "fieldName": "Id",
                                "comparisonType": "EQUAL",
                                "rightType": "CONSTANT",
                                "rightValue": id,
                                "rightValueType": "Guid"
                            }
                        ]
                },
                columnSet:  ["*"],
                objectType: "Contact",
                userId: id
            }
        )
            .then(function (response) {
                console.log(response);
                console.log("===================================!!======================================");
                var resObj = {},
                    arr = response.data.resultCollection[0].attributesCollection;
                // res.send(arr[6].name);
                for (var i = 0; i < arr.length; i++) {
                    console.log(arr[i].name );
                    if (arr[i].name === "Name") resObj.Name = arr[i].value;
                    if (arr[i].name === "JobTitle") resObj.JobTitle = arr[i].value;
                }
                resObj.points = getRandomInt(20, 100);

                res.render('page.ejs', {
                    link: 'https://banner2.kisspng.com/20180331/rcw/kisspng-bar-chart-computer-icons-font-awesome-computer-sof-statistics-5ac014462f81f6.7793666515225375421946.jpg',
                    Name: resObj.Name ? resObj.Name : "Вася Пупкин",
                    JobTitle: resObj.JobTitle ? resObj.JobTitle : "Бог",
                    points: resObj.points
                });
                // res.send(response.data);
            })
            .catch(function (error) {
                console.log(error);
                // res.send(error);
                console.log("===================================!!======================================");

            });

    });

    app.get('/registration',  function (req, res) {
        res.render('registration.ejs');
    });

    app.get('/task_hub', function (req, res) {
        res.redirect('/html/group_page.html');
    });


//router.post('/users', bodyParser, )

};

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}


// router.get('/user', bodyParser,  function(req, res) {
//     var query = url.parse(req.url, true).query;
// });