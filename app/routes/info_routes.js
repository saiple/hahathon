var bodyParser = require('body-parser').json(),
    bpmURL = "http://188.120.238.74:8085/api/v1/";
const axios = require('axios');
const indexRazil = require('./index_razil');


module.exports = function(app, fs) {
    indexRazil(app, fs);
    app.post('/register', bodyParser, function (req, res) {
        // var body = req.body;
        // console.log(body);
        // console.log(req.cookies);
        // postBPM("registration", body);
        // res.redirect("/");
        res.redirect(301, "/user?id=dad159f3-6c2d-446a-98d2-0f4d26662bbe");
        
    });
    app.post('/token', bodyParser, function (req, res, next) {
        // var body = req.body;
        //         // console.log(body);
        //         // console.log(req.cookies);
        //         // body.rememberMe = body.rememberMe === "on";
        //         // console.log(body);
        //         //
        //         // axios.post(bpmURL + "token", body)
        //         //     .then(function (response) {
        //         //         console.log(response);
        //         //         console.log("===================================!!======================================");
        //         //         res.cookie("Authorization", response.data.access_token);
        //         //         res.cookie("userId", response.data.userId);
        //         //         res.cookie("username", response.data.username);
        //         //         // next();
        //         //         // res.render('page.ejs')
        //         //         // res.redirect("/user");
        //         //         res.redirect(301,"/user?id=dad159f3-6c2d-446a-98d2-0f4d26662bbe");
        //         //     })
        //         //     .catch(function (error) {
        //         //         console.log(error);
        //         //         console.log("===================================!!======================================");
        //         //     });
        res.redirect(301, "/user?id=dad159f3-6c2d-446a-98d2-0f4d26662bbe");

    });
    app.post('/create', bodyParser, function (req, res, next) {
        var body = req.body;
        // console.log(body);
        console.log(req.cookies);
        // var cook = JSON.parse(req.cookies);
        body.userId = req.cookies.userId;



        axios.defaults.headers.common['Authorization'] = 'Bearer ' + req.cookies.Authorization;

        axios.post(bpmURL + "create", body)
            .then(function (response) {
                console.log(response);
                console.log("===================================!!======================================");
                res.send(response.data);

            })
            .catch(function (error) {
                console.log(error);
                console.log("===================================!!======================================");
            });

    });
    app.get('/tasks', function (req, res) {
        var url = require('url');
        var query = url.parse(req.url, true).query;

        console.log(req.cookies);
        console.log(req.url);
        console.log(query);

        var id = query.id ? query.id : req.cookies.userId;
        console.log(id);

        axios.defaults.headers.common['Authorization'] = 'Bearer ' + req.cookies.Authorization;
        axios.post(bpmURL + "readMultiple", {
                filterConnector: {
                    "subFiltersConnection": "And",
                    "subFilters":
                        [
                            {
                                "fieldName": "OwnerId",
                                "comparisonType": "EQUAL",
                                "rightType": "CONSTANT",
                                "rightValue": id,
                                "rightValueType": "Guid"
                            }
                        ]

                },
                sortRules: [
                    {
                        "name": "SolutionDate",
                        "sortDirection": "DESC"
                    }
                ]
            ,
                columnSet:  ["OwnerId", "StatusId", "SolutionDate", "Subject"],
                objectType: "Case",
                userId: id
            }
        )
            .then(function (response) {
                console.log(response);
                console.log("===================================!!======================================");
                res.send(response.data);
            })
            .catch(function (error) {
                console.log(error);
                // res.send(error);
                console.log("===================================!!======================================");

            });
        // res.redirect("html/registration.html");

    });
    app.get('/contacts', function (req, res) {
        var url = require('url');
        var query = url.parse(req.url, true).query;

        console.log(req.cookies);
        console.log(req.url);
        console.log(query);

        var id = query.id ? query.id : req.cookies.userId;
        console.log(id);

        axios.defaults.headers.common['Authorization'] = 'Bearer ' + req.cookies.Authorization;
        axios.post(bpmURL + "readMultiple", {
                filterConnector: {
                    "subFiltersConnection": "And",
                    "subFilters":
                        [
                            {
                                "fieldName": "Id",
                                "comparisonType": "EQUAL",
                                "rightType": "CONSTANT",
                                "rightValue": id,
                                "rightValueType": "Guid"
                            }
                        ]
                },
                columnSet:  ["Id", "Name", "PhotoId", "JobTitle"],
                objectType: "Contact",
                userId: id
            }
        )
            .then(function (response) {
                console.log(response);
                console.log("===================================!!======================================");
                res.send(response.data);
            })
            .catch(function (error) {
                console.log(error);
                // res.send(error);
                console.log("===================================!!======================================");

            });
        // res.redirect("html/registration.html");

    });
    app.get('/readMultiple', bodyParser, function (req, res) {
        var url = require('url');
        var query = url.parse(req.url, true).query;
        if (!query.objectType) {
            res.redirect("/html/test.html");
            return;
        }
        var q = query.objectType ? query.objectType : "SysImage";
        console.log(req.cookies);
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + req.cookies.Authorization;
        axios.post(bpmURL + "readMultiple", {
                filterConnector: {},
                sortRules: [],
                columnSet:  ["*"],
                objectType: q,
                userId: req.cookies.userId
            }
        )
            .then(function (response) {
                console.log(response);
                res.send(response.data);
            })
            .catch(function (error) {
                console.log(error);
            });
        // res.redirect("html/registration.html");

    });
    app.get('/stuff', bodyParser, function (req, res) {
        var url = require('url');
        var query = url.parse(req.url, true).query;
        var q = query.objectType ? query.objectType : "SysImage";
        console.log(req.cookies);
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + req.cookies.Authorization;
        axios.post(bpmURL + "readMultiple", {
                filterConnector: {},
                sortRules: [],
                columnSet:  ["Name", "JobTitle", "Id"],
                objectType: "Contact",
                userId: req.cookies.userId
            }
        )
            .then(function (response) {
                console.log(response);
                res.send(response.data);
            })
            .catch(function (error) {
                console.log(error);
            });
        // res.redirect("html/registration.html");

    });
    app.get('/test', function (req, res) {
        res.redirect("/html/test.html");
    });
};

function postBPM(method, params) {
    return axios.post(bpmURL + method, params)
        .then(function (response) {
            console.log(response);
            console.log("===================================!!======================================");
            return response;
        })
        .catch(function (error) {
            console.log(error);
            console.log("===================================!!======================================");
        });
}