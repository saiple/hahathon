// Объявляем четыре константы, каждая из констант
// содержит модуль из определенной библиотеки


const express = require('express');

const cookieParser = require('cookie-parser');

const app = express();

const fs = require('fs');

const bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());

require('./app/routes')(app, fs);


app.use(express.static('public'));
app.listen(80);
console.log("Server started at 80");