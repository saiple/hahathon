function tasker() {
    var data = [],
        id;
    try {
        var arr = window.location.href.split("?")[1].split("&"),
            params = {};
        for (var i = 0; i < arr.length; i++) {
            params[arr[i].split("=")[0]] = arr[i].split("=")[1];
        }
        id = params.id ? params.id : "dad159f3-6c2d-446a-98d2-0f4d26662bbe";
        console.log(params);
        console.log(arr);
    } catch(e) {
        id = "dad159f3-6c2d-446a-98d2-0f4d26662bbe";
    }

        template = document.querySelector(".task"),
        container = document.getElementById("container");
    // progress = 0;

    template.remove();
    function getData(id) {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', '/tasks?id=' + id, true);
        xhr.send(); // (1)
        xhr.onreadystatechange = function() {
            if (xhr.readyState != 4) return;
            if (xhr.status != 200) {
                console.log(xhr.status + ': ' + xhr.statusText);
            } else {
                console.log(xhr.responseText);
                data = JSON.parse(xhr.responseText).resultCollection;
                console.log(data);
                load(6);
                makeChart(data);
            }

        }
    }
    getData(id);

    function load(n) {
        if (progress >= data.length) {
            return;
        }
        for (var progress = 0; progress < data.length; progress++) {

            var cur = template.cloneNode(true);
            cur.querySelector(".title").innerHTML = "Task" + (progress + 1);
            var curData = data[progress].attributesCollection;
            console.log(data[progress].attributesCollection);

            // curData = curData.attributesCollection ? curData.attributesCollection : [];
            for (var i = 0; i < curData.length; i++) {
                console.log(curData[i].name);
                if (curData[i].name === "SolutionDate") {
                    cur.querySelector(".date").innerHTML = curData[i].value.split("T")[0] + " " + curData[i].value.split("T")[1];
                }
                if (curData[i].name === "Subject") {
                    cur.querySelector(".subject").innerText = curData[i].value;
                }
            }
            container.append(cur);
        }


    }

    return load;
}

function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

tasker();



/*===============================*/
    function makeChart(data) {
        //console.log(data);
        var time = [];


        for (var j = 0; j < data.length; j++) {
            var curData = data[j].attributesCollection;
            for (var i = 0; i < curData.length; i++) {
                console.log(curData[i].name);
                if (curData[i].name === "SolutionDate") {
                    time[j] = curData[i].value.split("T")[0];
                }

            }
        }
        var months = [];

        for(var i = 0; i < 12; i++){
            months[i] = 0;
        }
        console.log(time);
        for(var i = 0; i < time.length; i++){
            if (time[i].indexOf((new Date()).getFullYear().toString()) >= 0){
                months[Number(time[i].substring(5,7))]++;
            }
        }

        console.log(months);

        var ctx = document.getElementById("myChart").getContext('2d');
        //console.log(data);
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
                datasets: [{
                    label: 'tasks',
                    data: months,
                    backgroundColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)',
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });

    }
