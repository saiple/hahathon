var data = {};
var pr = 0;
var points = 100;
var template = document.querySelector(".item");
template.remove();
var container = document.getElementById("container");

function getData() {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', '/stuff', true);
    xhr.send(); // (1)
    xhr.onreadystatechange = function() {
        if (xhr.readyState != 4) return;
        if (xhr.status != 200) {
            console.log(xhr.status + ': ' + xhr.statusText);
        } else {
            console.log(xhr.responseText);
            data = JSON.parse(xhr.responseText).resultCollection;
            console.log(data);
            drawStuffData();
        }

    }
}
getData();

function drawStuffData() {
    var i = pr + 10;
    console.log(i);

    while(pr < i) {
        var cur = template.cloneNode(true);
        console.log(pr);
        console.log(data[pr]);
        var cur = template.cloneNode(true);
        cur.addEventListener('click', function () {
            window.open('/user?' + data[pr].attributesCollection[2].value);
        });
        cur.querySelector(".num").innerHTML = pr + 1;
        cur.querySelector(".name").innerHTML = data[pr].attributesCollection[0].value;
        cur.querySelector(".position").innerHTML = data[pr].attributesCollection[1].value;
        points -= getRandomInt(1,5);
        cur.querySelector(".points").innerHTML = points;
        container.appendChild(cur);
        pr++;
    }

}


function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}